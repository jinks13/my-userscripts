// ==UserScript==
// @description Redirects Bitchute Embeded URLs to Bitchute Video
// @name Bitchut Embeded Redirect 
// @namespace Backend
// @include http://www.bitchute.com/embed/*
// @include https://www.bitchute.com/embed/*
// @version 1.1
// @run-at document-start
// @grant none
// ==/UserScript==
var a = 0;
setInterval(function () {
	if (a === 0 && window.location.href.indexOf('watch?') > -1 && window.location.href.indexOf('list=WL') < 0) {
		a = '//bitchute.com/video?' + window.parent.location.href.split('?')[1];
		window.location.replace(a);
	}
}, 10);
